<?php

    /**
     * Uses the Phing DataType
     */
    require_once 'phing/types/DataType.php';

    /**
     * This Type represents a Phar Artifact.
     */
    class BodyType extends DataType {
        private $body;

        /** Supporting the <body>Message</body> syntax.
         * @param $body
         */
        public function addText($body)
        {
            $this->body = (string) $body;
        }

        /**
         * Getters
         */
        public function getBody(Project $p) {
            if ($this->isReference()) {
                return $this->getRef($p)->getBody($p);
            }

            return $this->body;
        }

        /**
         * Your datatype must implement this function, which ensures that there are
         * no circular references and that the reference is of the correct type.
         *
         * @return Artifact
         */
        public function getRef(Project $p) {
            if (!$this->checked) {
                $stk = array();
                array_push($stk, $this);
                $this->dieOnCircularReference($stk, $p);
            }
            $o = $this->ref->getReferencedObject($p);

            if (!($o instanceof Artifact)) {
                throw new BuildException($this->ref->getRefId() . " doesn't denote an Artifact");
            } else {
                return $o;
            }
        }
    }

?>
