<?php

/**
 * Uses the Phing DataType
 */
require_once 'phing/types/DataType.php';

/**
 * This Type represents a Phar Artifact.
 */
class ArtifactType extends DataType {
    private $group;
    private $name;
    private $version;

    /**
     * Sets the group of the artifact
     */
    public function setGroup($group) {
        $this->group = $group;
    }

    /**
     * Sets the name of the artifact
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Sets the version of the artifact
     */
    public function setVersion($version) {
        $this->version = $version;
    }

    /**
     * Getters
     */
    public function getGroup(Project $p) {
        if ($this->isReference()) {
            return $this->getRef($p)->getGroup($p);
        }

        return $this->group;
    }

    public function getName(Project $p) {
        if ($this->isReference()) {
            return $this->getRef($p)->getName($p);
        }

        return $this->name;
    }

    public function getVersion(Project $p) {
        if ($this->isReference()) {
            return $this->getRef($p)->getVersion($p);
        }

        return $this->version;
    }

    /**
     * Your datatype must implement this function, which ensures that there are
     * no circular references and that the reference is of the correct type.
     *
     * @return Artifact
     */
    public function getRef(Project $p) {
        if (!$this->checked) {
            $stk = array();
            array_push($stk, $this);
            $this->dieOnCircularReference($stk, $p);
        }
        $o = $this->ref->getReferencedObject($p);

        if (!($o instanceof Artifact)) {
            throw new BuildException($this->ref->getRefId() . " doesn't denote an Artifact");
        } else {
            return $o;
        }
    }
}
