<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';
include_once 'phing/types/FileList.php';
include_once 'phing/types/FileSet.php';

/**
 * Sample task that shows how you work with fileset, filelist and filterchain types.
 */
class ClassMapTask extends Task {
    protected $toFile = null; // the destination dir (from xml attribute)

    /**
     * Any filesets of files that should be appended.
     */
    private $filesets = array();

    /**
     * Any filelists of files that should be appended.
     */
    private $filelists = array();

    /**
     * Definition phar scheme
     */
    const PHAR_SCHEME = "phar://";

    /**
     * Supports embedded <filelist> element.
     * @return FileList
     */
    public function createFileList() {
        $num = array_push($this->filelists, new FileList());
        return $this->filelists[$num-1];
    }

    /**
     * Nested creator, adds a set of files (nested <fileset> attribute).
     * This is for when you don't care what order files get appended.
     * @return FileSet
     */
    public function createFileSet() {
        $num = array_push($this->filesets, new FileSet());
        return $this->filesets[$num-1];
    }

    /**
     * Set the toFile. We have to manually take care of the
     * type that is coming due to limited type support in php
     * in and convert it manually if neccessary.
     *
     * @param  string/object  The file, either a string or an PhingFile object
     * @return void
     * @access public
     */
    public function setTofile(PhingFile $toFile) {
        $this->toFile = $toFile;
    }

    /** Do work */
    public function main() {
        // append any files in filesets
        try {
            $phpExtension = ".php";
            $classArray = array();
            foreach ($this->filesets as $fs) {
                $files = $fs->getDirectoryScanner($this->project)->getIncludedFiles();
                foreach ($files as $file) {
                    if (strpos($file, $phpExtension) == (strlen($file)-strlen($phpExtension))) {
                        $fileName = end(explode(DIRECTORY_SEPARATOR, $file));
                        $className = substr($fileName, 0, strpos($fileName, $phpExtension));
                        if (strpos($className, '.') === false) {
                            //$file = str_replace("php-framework-", "phar://php-framework-", $file);
                            $file = str_replace("lib/", self::PHAR_SCHEME, $file);
                            $pathName = str_replace(DIRECTORY_SEPARATOR, "/", $file);
                            $classArray[$className] = $pathName;
                        }
                    }
                }
            }
        } catch (BuildException $be) {
            $this->log($be->getMessage(), Project::MSG_WARN);
        }

        $handle = fopen($this->toFile, "w");
        if ($handle) {
            fwrite($handle, "<?php\n\n");
            fwrite($handle, "\treturn array(\n");
            $i = 0;
            foreach ($classArray as $k => $v) {
                fwrite($handle, "\t\t\"".$k."\" => \"".$v."\"");
                if ($i < count($classArray)-1) {
                    fwrite($handle, ",");
                }
                fwrite($handle, "\n");
                $i++;
            }
            fwrite($handle, "\t);\n\n");
            fwrite($handle, "?>");
            fclose($handle);
        } else {
            echo "Cannot create and write to ClassMap.php";
        }
    }
}
