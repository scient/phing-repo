<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';
include_once 'phing/types/FileList.php';
include_once 'phing/types/FileSet.php';

/**
 * Sample task that shows how you work with fileset, filelist and filterchain types.
 */
class PharExtractTask extends Task {
    protected $toDir = null; // the destination dir (from xml attribute)

    /** Any filesets of files that should be appended. */
    private $filesets = array();

    /** Any filelists of files that should be appended. */
    private $filelists = array();

    /**
     * Supports embedded <filelist> element.
     * @return FileList
     */
    public function createFileList() {
        $num = array_push($this->filelists, new FileList());
        return $this->filelists[$num-1];
    }

    /**
     * Nested creator, adds a set of files (nested <fileset> attribute).
     * This is for when you don't care what order files get appended.
     * @return FileSet
     */
    public function createFileSet() {
        $num = array_push($this->filesets, new FileSet());
        return $this->filesets[$num-1];
    }

    /**
     * Set the toDir. We have to manually take care of the
     * type that is coming due to limited type support in php
     * in and convert it manually if neccessary.
     *
     * @param  string/object  The directory, either a string or an PhingFile object
     * @return void
     * @access public
     */
    public function setTodir(PhingFile $toDir) {
        $this->toDir = $toDir;
    }

    /** Do work */
    public function main() {
        // append the files in the filelists
/*			foreach($this->filelists as $fl) {
            try {
                $files = $fl->getFiles($this->project);
                foreach ($files as $file) {
                    echo "filelists: ".$file."\n";
                }
            } catch (BuildException $be) {
                $this->log($be->getMessage(), Project::MSG_WARN);
            }
        }
*/
        // append any files in filesets
        foreach ($this->filesets as $fs) {
            try {
                $files = $fs->getDirectoryScanner($this->project)->getIncludedFiles();
                $libDir = $fs->getDir($this->project);
                foreach ($files as $file) {
                    $fileName = substr($file, 0, strpos($file, ".phar"));
                    $p = new Phar($libDir."/".$file, 0, $file);
                    $p->extractTo($this->toDir."/".$fileName, null, true);
                }
            } catch (BuildException $be) {
                $this->log($be->getMessage(), Project::MSG_WARN);
            }
        }
    }
}
