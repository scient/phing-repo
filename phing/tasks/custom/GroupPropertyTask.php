<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';

/**
 * Task to fetch properties with a group prefix followed by _.
 *
 * @author Ivo Krooswjik
 */
class GroupPropertyTask extends Task {
    /**
     * Group prefix
     *
     * @var  string
     */
    private $group;

    /**
     * Property key
     *
     * @var  string
     */
    private $property;

    /**
     * Set the group name.
     *
     * @param  string  The group name
     * @return void
     * @access public
     */
    function setGroup($group) {
        $this->group = $group;
    }

    /**
     * Set the property key.
     *
     * @param  string  The property key
     * @return void
     * @access public
     */
    function setProperty($property) {
        $this->property = $property;
    }

    /**
     * The main entry point method.
     */
    function main() {
        $envPrefix = "env";

        $key = $envPrefix . "." . $this->property;
        $val = $this->project->getProperty($envPrefix . "." . $this->group . "_" . $this->property);

        $this->project->setProperty($key, $val);
    }
}
