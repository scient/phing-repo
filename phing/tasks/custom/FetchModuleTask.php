<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';
include_once 'phing/types/custom/ArtifactType.php';
include_once 'phing/tasks/ext/HttpGetTask.php';
include_once 'HTTP/Request2.php';

/**
 * Sample task that shows how you work with fileset, filelist and filterchain types.
 */
class FetchModuleTask extends Task {
    protected $toDir = null; // the destination dir (from xml attribute)
    protected $repo = null; // the destination dir (from xml attribute)

    /**
     * Any artifacts that should be fetched.
     */
    private $artifacts = array();

    /**
     * Supports embedded <filelist> element.
     * @return FileList
     */
    public function createArtifact() {
        $num = array_push($this->artifacts, new ArtifactType());
        return $this->artifacts[$num-1];
    }

    /**
     * Set the toFile. We have to manually take care of the
     * type that is coming due to limited type support in php
     * in and convert it manually if neccessary.
     *
     * @param  string/object  The file, either a string or an PhingFile object
     * @return void
     * @access public
     */
    public function setToDir(PhingFile $toDir) {
        $this->toDir = $toDir;
    }

    /**
     * Set the repo.
     *
     * @param  string/object  The repository host name
     * @return void
     * @access public
     */
    public function setRepo($repo) {
        $this->repo = $repo;
    }

    /**
     * The main entry point method.
     */
    public function main() {
        // append any files in filesets
        try {
            $project = $this->project;
            foreach ($this->artifacts as $art) {
                $fileName = $art->getGroup($project)."-".$art->getName($project)."-".$art->getVersion($project).".phar";
                $download = $this->repo."/".
                            $art->getGroup($project)."/".
                            $art->getName($project)."/".
                            $art->getVersion($project)."/".
                            $fileName;

                $this->downloadFile($download, $this->toDir."/".$fileName);
                $this->log("Fetching module: ".$download);
            }
        } catch (BuildException $be) {
            $this->log($be->getMessage(), Project::MSG_WARN);
        }
    }

    private function downloadFile($file, $destination) {
        $request = new HTTP_Request2($file, HTTP_Request2::METHOD_GET);
        $fp = fopen($destination, "w+");
        try {
            $response = $request->send();
            if (200 == $response->getStatus()) {
                fwrite($fp, $response->getBody());
            } else {
                echo "Unexpected HTTP status: ".$response->getStatus()." ".$response->getReasonPhrase();
            }
        } catch (HTTP_Request2_Exception $e) {
            echo "Error: ".$e->getMessage();
        }
        fclose($fp);
    }
}
