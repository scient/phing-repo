<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';

/**
 * PackageJson task
 *
 * @author    Ivo Krooswijk
 * @version   $Revision$
 * @package   phing.tasks.custom
 */
class PackageJsonTask extends Task {
    /**
     * The location passed in the buildfile.
     */
    protected $loc = ".";

    /**
     * The name of the package.json to be read.
     */
    protected $packageFile = "package.json";

    /**
     * The setter for the attribute "location"
     * @param string $loc
     */
    public function setLoc($loc) {
        $this->loc = $loc;
    }

    /**
     * The setter for the attribute "packageFile"
     * @param string $packageFile
     */
    public function setPackageFile($packageFile) {
        $this->packageFile = $packageFile;
    }

    /**
     * The init method: Do init steps.
     */
    public function init() {
        // nothing to do here
    }

    /**
     * The main entry point method.
     */
    public function main() {
        $file = $this->loc."/".$this->packageFile;
        $package = file_get_contents($file);
        $package_json = json_decode($package);

        $name = $package_json->name;
        $version = $package_json->version;
        $description = $package_json->description;
        $author = $package_json->author;

        $this->log("Read from: " . $file);
        $this->log("Version found: " . $version);
        $this->log("Author found: " . $author);
        $this->log("Name found: " . $name);
        $this->log("Description found: " . $description);

        $this->project->setProperty('application.name', $name);
        $this->project->setProperty('application.version', $version);
        $this->project->setProperty('application.description', $description);
        $this->project->setProperty('application.author', $author);
    }
}
