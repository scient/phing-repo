<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';
include_once 'phing/types/FileList.php';
include_once 'phing/types/FileSet.php';

/**
 * Sample task that shows how you work with fileset, filelist and filterchain types.
 */
class TestComplexTask extends Task {
    /** Any filesets of files that should be appended. */
    private $filesets = array();

    /** Any filelists of files that should be appended. */
    private $filelists = array();

    /** Any filters to be applied before append happens. */
    private $filterChains = array();

    /**
     * Supports embedded <filelist> element.
     * @return FileList
     */
    public function createFileList() {
        $num = array_push($this->filelists, new FileList());
        return $this->filelists[$num-1];
    }

    /**
     * Nested creator, adds a set of files (nested <fileset> attribute).
     * This is for when you don't care what order files get appended.
     * @return FileSet
     */
    public function createFileSet() {
        $num = array_push($this->filesets, new FileSet());
        return $this->filesets[$num-1];
    }

    /**
     * Creates a filterchain
     *
     * @return FilterChain The created filterchain object
     */
    public function createFilterChain() {
        $num = array_push($this->filterChains, new FilterChain($this->project));
        return $this->filterChains[$num-1];
    }

    /** Do work */
    public function main() {
        // append the files in the filelists
        foreach($this->filelists as $fl) {
            try {
                $files = $fl->getFiles($this->project);
                foreach ($files as $file) {
                    $in = FileUtils::getChainedReader(new FileReader($file), $this->filterChains, $this->project);
                    while (-1 !== ($buffer = $in->read())) {
                        // $buffer now contains the end-result chunk of filtered text that can be put in another file, etc.
                        // (Do whatever your task needs to do here...)
                    }
                }
            } catch (BuildException $be) {
                $this->log($be->getMessage(), Project::MSG_WARN);
            }
        }

        // append any files in filesets
        foreach($this->filesets as $fs) {
            try {
                $files = $fs->getDirectoryScanner($this->project)->getIncludedFiles();
                foreach ($files as $file) {
                    $in = FileUtils::getChainedReader(new FileReader($file), $this->filterChains, $this->project);
                    while (-1 !== ($buffer = $in->read())) {
                        // $buffer now contains the end-result chunk of filtered text that can be put in another file, etc.
                        // (Do whatever your task needs to do here...)
                    }
                }
            } catch (BuildException $be) {
                $this->log($be->getMessage(), Project::MSG_WARN);
            }
        }
    }
}
