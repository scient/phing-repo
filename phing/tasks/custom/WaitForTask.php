<?php

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';

/**
 *
 * @author    Ivo Krooswijk
 * @version   $Revision$
 * @package   phing.tasks.custom
 */
class WaitForTask extends ConditionBase {
    const ONE_MILLISECOND = 1;
    const ONE_SECOND = 1000;
    const ONE_MINUTE = 60000;
    const ONE_HOUR = 3600000;
    const ONE_DAY = 86400000;
    const ONE_WEEK = 604800000;

    const DEFAULT_MAX_WAIT_MILLIS = 180000;
    const DEFAULT_CHECK_MILLIS = 500;

    protected $maxWait = self::DEFAULT_MAX_WAIT_MILLIS;
    protected $maxWaitMultiplier = self::ONE_MILLISECOND;

    protected $checkEvery = self::DEFAULT_CHECK_MILLIS;
    protected $checkEveryMultiplier = self::ONE_MILLISECOND;

    protected $timeoutProperty = null;

    /**
     * Set the maximum length of time to wait.
     * @param int $maxWait
     */
    public function setMaxWait($maxWait) {
        $this->maxWait = (int)$maxWait;
    }

    /**
     * Set the max wait time unit
     * @param string $maxWaitUnit
     */
    public function setMaxWaitUnit($maxWaitUnit) {
        $this->maxWaitMultiplier = $this->_convertUnit($maxWaitUnit);
    }

    /**
     * Set the time between each check
     * @param int $checkEvery
     */
    public function setCheckEvery($checkEvery) {
        $this->checkEvery = (int)$checkEvery;
    }

    /**
     * Set the check every time unit
     * @param string $checkEveryUnit
     */
    public function setCheckEveryUnit($checkEveryUnit) {
        $this->checkEveryMultiplier = $this->_convertUnit($checkEveryUnit);
    }

    /**
     * Name of the property to set after a timeout.
     * @param string $timeoutProperty
     */
    public function setTimeoutProperty($timeoutProperty) {
        $this->timeoutProperty = $timeoutProperty;
    }

    /**
     * Convert the unit to a multipler.
     * @param string $unit
     */
    protected function _convertUnit($unit) {
        switch ($unit) {
            case "week": {
                return self::ONE_WEEK;
            }

            case "day": {
                return self::ONE_DAY;
            }

            case "hour": {
                return self::ONE_HOUR;
            }

            case "minute": {
                return self::ONE_MINUTE;
            }

            case "second": {
                return self::ONE_SECOND;
            }

            case "millisecond": {
                return self::ONE_MILLISECOND;
            }

            default: {
                throw new BuildException("Illegal unit '$unit'");
            }
        }
    }

    /**
     * Check repeatedly for the specified conditions until they become
     * true or the timeout expires.
     * @throws BuildException
     */
    public function main() {
        if ($this->countConditions() > 1) {
            throw new BuildException("You must not nest more than one condition into <waitfor>");
        }

        if ($this->countConditions() < 1) {
            throw new BuildException("You must nest a condition into <waitfor>");
        }

        $cs = $this->getIterator();
        $condition = $cs->current();

        $maxWaitMillis = $this->maxWait * $this->maxWaitMultiplier;
        $checkEveryMillis = $this->checkEvery * $this->checkEveryMultiplier;

        $start = microtime(true) / 1000;
        $end = $start + $maxWaitMillis;

        while (microtime(true) / 1000 < $end) {
            if ($condition->evaluate()) {
                $this->log(getTaskName() + ": condition was met", Project.MSG_VERBOSE);

                return;
            }

            usleep($checkEveryMillis * 1000);
        }

        $this->log(getTaskName() + ": timeout", Project.MSG_VERBOSE);

        if ($tihs->timeoutProperty != null) {
            $this->project->setNewProperty($this->timeoutProperty, "true");
        }
    }
}
