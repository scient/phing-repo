<?php

/*
<taskdef name="minify" classname="extended.tasks.kpMinTask" />

<target name="minify-js">
    <echo>Minify javascript to release</echo>
    <minify targetDir="../www/resources/js/min" yuiPath="tools/yuicompressor.jar">
        <fileset dir="../www/resources/js/">
            <include name="*.js"/>
        </fileset>
    </minify>
</target>

<target name="minify-css">
    <echo>Minify CSS to release</echo>
    <minify targetDir="../www/resources/v2_css/min" yuiPath="tools/yuicompressor.jar">
        <fileset dir="../www/resources/v2_css/">
            <include name="*.css"/>
        </fileset>
    </minify>
</target>
*/

/**
 * Uses the Phing Task
 */
require_once 'phing/Task.php';

/**
 * Task to compress files using YUI Compressor.
 *
 * @author Ivo Krooswjik
 */
class MinimizeTask extends Task {
    /**
     * path to YuiCompressor
     *
     * @var  string
     */
    protected $yuiPath;

    /**
     * type of file to compress
     *
     * @var  string
     */
    protected $type;

    /**
     * the source files
     *
     * @var  FileSet
     */
    protected $filesets = array();

    /**
     * Whether the build should fail, if
     * errors occured
     *
     * @var boolean
     */
    protected $failonerror = false;

    /**
     * directory to put minified javascript files into
     *
     * @var  string
     */
    protected $targetDir;

    /**
     * sets the path where JSmin can be found
     *
     * @param  string  $yuiPath
     */
    public function setYuiPath($yuiPath) {
        $this->yuiPath = $yuiPath;
    }

    /**
     * sets the type of the file to compress
     *
     * @param  string  $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     *  Nested creator, adds a set of files (nested fileset attribute).
     */
    public function createFileSet() {
        $num = array_push($this->filesets, new FileSet());
        return $this->filesets[$num - 1];
    }

    /**
     * Whether the build should fail, if an error occured.
     *
     * @param boolean $value
     */
    public function setFailonerror($value) {
        $this->failonerror = $value;
    }

    /**
     * sets the directory compressor traget dir
     *
     * @param  string  $targetDir
     */
    public function setTargetDir($targetDir) {
        $this->targetDir = $targetDir;
    }

    /**
     * The init method: Do init steps.
     */
    public function init() {
        return true;
    }

    /**
     * The main entry point method.
     */
    public function main() {
        $command = "java -jar {yuipath} {type} {src} -o {target}";

        foreach ($this->filesets as $fs) {
            try {
                $files = $fs->getDirectoryScanner($this->project)->getIncludedFiles();
                $fullPath = realpath($fs->getDir($this->project));

                foreach ($files as $file) {
                    $this->log("Minifying file ".$file);

                    $target = $this->targetDir."/".str_replace($fullPath, "", $file);

                    if (file_exists(dirname($target)) == false) {
                        mkdir(dirname($target), 0700, true);
                    }

                    $cmd = str_replace('{src}', '"'.$fullPath.DIRECTORY_SEPARATOR.$file.'"', $command);
                    $cmd = str_replace('{target}', '"'.$target.'"', $cmd); // realpath($target)
                    $cmd = str_replace('{yuipath}', realpath($this->yuiPath), $cmd);

                    if (in_array($this->type, array("js", "css"))) {
                        $cmd = str_replace('{type}', "--type=".$this->type, $cmd);
                    }

                    $output = array();
                    $return = null;

                    $this->log("Executing command ".$cmd);

                    exec($cmd, $output, $return);

                    foreach ($output as $line) {
                        $this->log($line, Project::MSG_VERBOSE);
                    }

                    if ($return != 0) {
                        throw new BuildException("Task exited with code $return");
                    }
                }
            } catch (BuildException $be) {
                // directory doesn't exist or is not readable
                if ($this->failonerror) {
                    throw $be;
                } else {
                    $this->log($be->getMessage(), $this->quiet ? Project::MSG_VERBOSE : Project::MSG_WARN);
                }
            }
        }
    }
}
